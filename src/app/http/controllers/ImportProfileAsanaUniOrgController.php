<?php

namespace Yeltrik\ImportProfileAsanaUniOrg\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImportProfileAsanaUniOrgController extends Controller
{

    /**
     * ImportProfileAsanaUniOrgController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    public function create()
    {
        // TODO Authenticate
        return view('importProfileAsanaUniOrg::import.profile.asana.uni-org.create');
    }
}
