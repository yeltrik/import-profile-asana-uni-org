<form
    method="POST"
    action="{{ action([\Yeltrik\ImportProfileAsana\app\http\controllers\ImportProfileAsanaCsvController::class, 'store']) }}"
    enctype="multipart/form-data"
>
    @csrf

    <hr>
    <h3>
        Profile
    </h3>
    @include('importProfileAsana::import.profile.asana.input.all')

    <hr>

    <h3>
        University
    </h3>
    <h4>
        University Organization
    </h4>
    @include('importProfileAsanaUniOrg::import.profile.asana.uni-org.input.group.uni_org')

    <br>
    @include('importProfileAsana::import.profile.asana.input.import_button')

</form>
