@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>
            Import Profile(s) from Asana as an University Membership
        </h2>

        @include("importProfileAsanaUniOrg::import.profile.asana.uni-org.form.import")

    </div>
@endsection
