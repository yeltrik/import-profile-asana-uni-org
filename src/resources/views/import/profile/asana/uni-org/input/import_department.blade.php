<div class="input-group mb-3">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input
                type="checkbox"
                id="import_department"
                name="import_department"
                aria-label="Checkbox for importing Departments"
                disabled
            >
        </div>
    </div>
    <label class="input-group-text" for="import_department">Import Department</label>
</div>
