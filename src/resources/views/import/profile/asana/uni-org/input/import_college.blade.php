<div class="input-group mb-3">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input
                type="checkbox"
                id="import_college"
                name="import_college"
                aria-label="Checkbox for importing Colleges"
                disabled
            >
        </div>
    </div>
    <label class="input-group-text" for="import_college">Import College</label>
</div>
