<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportProfileAsanaUniOrg\app\http\controllers\ImportProfileAsanaUniOrgController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('import/profile/asana/uni-org/create',
    [ImportProfileAsanaUniOrgController::class, 'create'])
    ->name('imports.profiles.asana.uni-org.create');

Route::post('import/profile/asana/uni-org',
    [ImportProfileAsanaUniOrgController::class, 'store'])
    ->name('imports.profiles.asana.uni-org.store');
